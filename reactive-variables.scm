;; Simplest Reactive System Ever in R5RS (with lifting)
;; Inspired by `Schuster, C., & Flanagan, C. (2016, March). Reactive programming with reactive variables. In Companion Proceedings of the 15th International Conference on Modularity (pp. 29-33).`
;; This implementation: (C) Bjarno Oeyen 2023

;; As a convention...
;; - *variable*'s are not meant to be accessed directly
;; - $procedure's are used for procedures/syntax to make a reactive program (see example)
;; - %variable's are variables containing signals (either source or derived signals, also see example)

;; *signal* :: any, bool, (/ -> any) U {#f} -> signal
(define *signal*
  (let ((next-pair (cons #f #f))) ;; Signals are chained in construction.
    (lambda (v s u)
      (let* ((c next-pair)
             (n (cons #f #f))
             (r (lambda args
                  (let ((arg (if (null? args) 'get (car args))))
                    (cond ((eq? arg 'get) v)
                          ((eq? arg 'setter) (if s (lambda (v2) (set! v v2)) #f))
                          ((eq? arg 'updater) (if u (lambda () (set! v (u))) #f))
                          ((eq? arg 'chain) c)
                          (else #f))))))
        (set-car! c r)
        (set-cdr! c n)
        (set! next-pair n)
        r))))

;; $source :: any -> signal
(define ($source val)
  (*signal* val #t #f))

;; [syntax] $derived :: <expr> -> signal
(define-syntax $derived
  (syntax-rules ()
    (($derived expr)
     (let ((f (lambda () expr)))
       (*signal* (f) #f f)))))

;; $! :: signal, any -> /
(define ($! src val)
  (let ((setter (src 'setter)))
    (if setter
        (begin
          (setter val)
          (let loop ((ptr (cdr (src 'chain))))
            (if (car ptr)
                (let ((u ((car ptr) 'updater)))
                  (if u (u)) ;; sources don't have updaters
                  (loop (cdr ptr)))))))))


;;;;;;;;;;;;;
;; Example ;;
;;;;;;;;;;;;;

;; 1. Create a new source
(define %time ($source 0))

;; 2. Create a new derived signal
(define %fast-time ($derived (* (%time) 2)))

;; 3. Update the source
($! %time 10)

;; 4. Derived signal should now be updated.
(display (%fast-time))