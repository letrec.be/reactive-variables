# Simplest Reactive System Ever in R5RS (with lifting)
## Reactive Variables

Scheme (R5RS) implementation that implements the principles of _Schuster, C., & Flanagan, C. (2016, March). Reactive programming with reactive variables. In Companion Proceedings of the 15th International Conference on Modularity (pp. 29-33)._.

Its not meant to be useful for _actual_ application development, but may serve as a reasoning device to better understand different kinds of reactive programming languages. This is the simplest implementation that I could think of that implements a form of _reactivity_ (i.e. automatic re-computation of signals). The signal update order is simply derived from the signal creation order. It is assumed that a signal can only be created, and depend on, signals that were created earlier.

This simple implementation has no support for any form of dynamicity (higher-order signals are supported, but there is nothing that resembles "switching in/out" these signals), nor is there any support for any stateful accumulation of data.